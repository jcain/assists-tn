# Assists TN

Assists TN is a library of classes and functions to assist common operations on native types in PHP 7.4+.


## Installation

```batchfile
composer require jcain/assists-tn
```


## Component stability statuses

| Component                         | Stability | Since |
|:----------------------------------|:---------:|:-----:|
| [RegExp](src/RegExp.api.md)       | alpha     | 0.0   |
| [TArray](src/TArray.api.md)       | alpha     | 0.0   |
| [Timestamp](src/Timestamp.api.md) | alpha     | 0.0   |
| [TInt](src/TInt.api.md)           | alpha     | 0.0   |
| [TString](src/TString.api.md)     | alpha     | 0.0   |
| [Url](src/Url.api.md)             | alpha     | 0.0   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

Assists TN is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.