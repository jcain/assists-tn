<?php namespace JCain\Assists\TN;


class Timestamp {
	private function __construct() {
		// Do nothing.
	}


	static public function compact($ms = true, $time = null) {
		if ($ms) {
			if ($time === null)
				$time = microtime(true);
			$ms = (int)(($time - (int)$time) * 1000);

			return gmdate('YmdHis', $time) . ($ms > 99 ? '' : ($ms > 9 ? '0' : '00')) . $ms;
		}
		else {
			return ($time === null ? gmdate('YmdHis') : gmdate('YmdHis', $time));
		}
	}


	static public function iso8601($ms = true, $time = null) {
		if ($ms) {
			if ($time === null)
				$time = microtime(true);
			$ms = (int)(($time - (int)$time) * 1000);

			return gmdate('Y-m-d\TH:i:s', $time) . '.' . ($ms > 99 ? '' : ($ms > 9 ? '0' : '00')) . $ms . 'Z';
		}
		else {
			return ($time === null ? gmdate('Y-m-d\TH:i:s\Z') : gmdate('Y-m-d\TH:i:s\Z', $time));
		}
	}
}