<?php namespace JCain\Assists\TN;


class RegExp {
	static private $chrs;

	private $pattern;
	private $global;


	public function __construct($pattern, $flags = '') {
		$flags = str_replace('g', '', $flags, $count);
		if ($count)
			$this->global = true;

		$this->pattern = '/' . $pattern . '/' . $flags;
	}


	public function search($str) {
		return (preg_match($this->pattern, $str, $matches, PREG_OFFSET_CAPTURE) ? $matches[1] : -1);
	}


	public function replace($str, $value) {
		return preg_replace($this->pattern, $value, $str, ($this->global ? -1 : 1));
	}


	/// Escapes any regular expression character that appears in the string.
	/// For example, it replaces '*' with '\*'.
	/// @params str : A regular expression string.
	/// @returns The modifed string.
	static public function escape($str) {
		if (!self::$chrs)
			self::init();
		return preg_replace(self::$chrs, "\\\\$1", $str);
	}


	static private function init() {
		$chrs = [
			'.', '*', '+', '?', '|', '^', '$',
			'(', ')', '[', ']', '{', '}', '\\', '/'
		];
		self::$chrs = '/(\\' . implode('|\\', $chrs) . ')/';
	}
}