<?php namespace JCain\Assists\TN;


class TArray {
	private function __construct() {
		// Do nothing.
	}


	static public function isAssoc(array $array) {
		foreach ($array as $key => $value) {
			if (is_string($key))
				return true;
		}
		return false;
	}


	static public function isAssocCheap(array $array) {
		foreach ($array as $key => $value)
			return is_string($key);
		return false;
	}
}