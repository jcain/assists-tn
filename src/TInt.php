<?php namespace JCain\Assists\TN;


class TInt {
	public const INT8S_MIN = -128;
	public const INT8S_MAX = 127;
	public const INT8U_MAX = 255;
	public const INT16S_MIN = -32768;
	public const INT16S_MAX = 32767;
	public const INT16U_MAX = 65535;
	public const INT24S_MIN = -8388608;
	public const INT24S_MAX = 8388607;
	public const INT24U_MAX = 16777215;
	public const INT32S_MIN = -2147483648;
	public const INT32S_MAX = 2147483647;
	public const INT32U_MAX = 4294967295;
	public const INT48S_MIN = -140737488355328;
	public const INT48S_MAX = 140737488355327;
	public const INT48U_MAX = 281474976710655;
	public const INT64S_MIN = -9223372036854775808;
	public const INT64S_MAX = 9223372036854775807;
	public const INT64U_MAX = 18446744073709551615;


	private function __construct() {
		// Do nothing.
	}
}