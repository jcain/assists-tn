<?php namespace JCain\Assists\TN;


class TString {
	public const UTF8_BOM = "\xEF\xBB\xBF";
	public const UTF8_MAX_CHAR_3 = "\xEF\xBF\xBF";
	public const UTF8_MAX_CHAR_4 = "\xF7\xBF\xBF\xBF";

	public const STR_LTRIM = 0x01;
	public const STR_RTRIM = 0x02;
	public const STR_SQUEEZE = 0x04;
	public const STR_INLINE = 0x08;
	public const STR_TCROP = 0x10;
	public const STR_BCROP = 0x20;
	public const STR_SCRUNCH = 0x40;

	public const STR_TRIM = self::STR_LTRIM | self::STR_RTRIM;
	public const STR_CROP = self::STR_TCROP | self::STR_BCROP;

	public const STR_DEFAULT = self::STR_TRIM | self::STR_SQUEEZE | self::STR_INLINE;


	private function __construct() {
		// Do nothing.
	}


	static public function contains($str, $frag) {
		if ($frag === '')
			return true;
		else if ($str === '')
			return false;

		return (strpos($str, $frag) !== false);
	}


	/// Checks whether the first part of one string is equal to another string.
	static public function startsWith($str, $frag) {
		if ($frag === '')
			return true;
		else if ($str === '')
			return false;

		return (strpos($str, $frag) === 0);
	}


	/// Checks whether the last part of one string is equal to another string.
	static public function endsWith($str, $frag) {
		if ($frag === '')
			return true;
		else if ($str === '')
			return false;

		$index = strlen($str) - strlen($frag);
		return (strrpos($str, $frag) === $index);
	}


	/// Removes extras from a string, some options depending on which flags are set.
	static public function normalize($value, $null = null, $flags = self::STR_DEFAULT) {
		if ($value === null || $value === '')
			return ($null !== null ? ($null ? null : '') : $value);

		if (!is_string($value))
			throw new \InvalidArgumentException("\$value : Invalid type '" . gettype($value) . "'");
		if (!is_integer($flags) && !is_float($flags))
			throw new \InvalidArgumentException("\$flags : Invalid type '" . gettype($flags) . "'");

		// Remove unwelcome control characters.
		$value = preg_replace('/[\x00-\x08\x0E-\x1F]/u', '', $value);
		// Replace the various line break forms with newline.
		$value = preg_replace('/(\x0B|\f|\r\n?)/u', "\n", $value);

		if ($flags & self::STR_INLINE)
			$value = preg_replace('/\n+/u', ' ', $value);

		if ($flags & self::STR_SQUEEZE)
			$value = preg_replace('/(?<=\S)\h+(?=\S)/um', ' ', $value);

		if ($flags & self::STR_RTRIM)
			$value = preg_replace('/^\h+/um', '', $value);

		if ($flags & self::STR_LTRIM)
			$value = preg_replace('/\h+$/um', '', $value);

		if (!($flags & self::STR_INLINE)) {
			if ($flags & self::STR_SCRUNCH) {
				$value = preg_replace('/\n(\h*\n)+(?=\h*\S)/u', "\n\n", $value);
				$value = preg_replace('/(?<!\n)\n(?!(\n|$))/u', ' ', $value);
			}

			if ($flags & self::STR_TCROP)
				$value = preg_replace('/^(\h*\n)*/u', '', $value);

			if ($flags & self::STR_BCROP)
				$value = preg_replace('/(\n\h*)*$/u', '', $value);
		}

		if ($value === '' && $null)
			$value = null;

		return $value;
	}


	static public function jsquotes($str, $quot = '') {
		$str = str_replace("\\", "\\\\", $str);
		$str = str_replace("'", "\\'", $str);
		$str = str_replace('"', '\\"', $str);
		$str = str_replace("&", "&amp;", $str);
		$str = str_replace("'", "&#39;", $str);
		$str = str_replace('"', '&#34;', $str);
		return $quot . $str . $quot;
	}


	static public function strposeol($haystack, $needle, $offset = 0) {
		$index = strpos($haystack, $needle, $offset);
		return ($index !== false ? $index : strlen($haystack));
	}


	static public function substrrpos($haystack, $needle, $offset = 0) {
		$index = strrpos($haystack, $needle, $offset) + strlen($needle);
		return ($index !== false ? substr($haystack, $index) : '');
	}
}