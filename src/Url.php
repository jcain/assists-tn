<?php namespace JCain\Assists\TN;


class Url {
	private function __construct() {
		// Do nothing.
	}


	static public function getDomain($level = 2) {
		preg_match('/\.?((\w+\.){1,' . ($level - 1) . '}\w+)$/', $_SERVER['SERVER_NAME'], $matches);
		return $matches[1];
	}


	/// Parses the URL query string into an array of arrays.
	static public function parseQueryString() {
		$params = [];

		$query = $_SERVER["QUERY_STRING"];
		if ($query != "") {
			$query = explode("&", $query);

			foreach ($query as $param) {
				if ($param != "") {
					list($name, $value) = explode("=", $param);
					$params[urldecode($name)][] = urldecode($value);
				}
			}
		}

		return $params;
	}


	static public function rawurlparamencode($url) {
		$url = str_replace("%", "%25", $url);
		$url = str_replace("&", "%26", $url);
		$url = str_replace(";", "%3B", $url);
		$url = str_replace("=", "%3D", $url);
		return $url;
	}
}